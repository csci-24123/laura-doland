<?php

//function to clean the data that's input in form
function clean_input($data) {
    $data = trim($data); // removes whitespace
    $data = stripslashes($data); // strips slashes
    $data = htmlspecialchars($data); // replaces html chars
    return $data;
} //closes clean input function

//function to connect to db
function connect_to_db($dbName) {
    $servername = "localhost";
    $username = "root";
    $password = "";
    try {
        return new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);      
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}// closes out connect function

//Function to login the user from the login page and direct to splash
function login($userLogin) {
    $_SESSION['userLogin'] = $userLogin;
    $userId = getUserId($userLogin);
    $_SESSION['userId'] = $userId;
    header("Location: splash.php");
} // closes out login function

// function to get the User Id from their name
function getUserId($userFullName) {
    $conn = connect_to_db ("finalprojectlauradoland");
    $select = "SELECT userId from users WHERE userFullName=:userFullName";
    $stmt = $conn->prepare($select);
    $stmt->bindParam(':userFullName', $userFullName);
    $stmt->execute();
 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
    return $listRow['userId'];
    }
} // closes out get user Id function