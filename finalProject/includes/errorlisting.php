<?php
class Listing {
    // parameters
    public $conn;
    public $errorId;
    public $errorNotes;
    public $whatNeedsDone;
    public $responsibleParty;
    public $reportedDate;
    public $resolved;
    public $primaryImage;
    public $imageTitle;
 
    //creates one to one match between database and Listing class
    function __construct($conn, $errorInfo) {
        $this->conn = $conn;
        $this->errorId = $errorInfo['errorId'];
        $this->errorNotes = $errorInfo['errorNotes'];
        $this->whatNeedsDone = $errorInfo['whatNeedsDone'];
        $this->responsibleParty = $errorInfo['responsibleParty'];
        $this->reportedDate = $errorInfo['reportedDate'];
        $this->resolved = $errorInfo['resolved'];
        $this->primaryImage = $errorInfo['primaryImage'];
        $this->imageTitle = $errorInfo['imageTitle'];
    } // closes out construct function
 
    function __destruct() {}

    // pull in the error information from the db
    static function getErrorsFromDb($conn) {
        $selectErrors = "SELECT * FROM errorLog";
        $stmt = $conn->prepare($selectErrors);
        $stmt->execute();
           
        $errorList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        
        foreach($stmt->fetchAll() as $listRow) {
            $errorInfo = NEW Listing($conn, $listRow);
            $errorList[] = $errorInfo;
        } // closes out foreach loop
         
        return $errorList;
    }  // closes out get ErrorsFromDb function
   

    //Pulls in the error information from the db by errorId
    static function getErrorById($conn, $errorId) {
        //$selectErrors = "SELECT errorLog.*, users.userFullName AS responsibleParty FROM errorLog LEFT JOIN (users) 
        //ON users.userId=errorLog.responsibleParty WHERE errorLog.errorId =:errorId";
        $selectErrors = "SELECT * FROM errorLog WHERE errorLog.errorId=:errorId";

        $stmt = $conn->prepare($selectErrors);
        $stmt->bindParam(':errorId', $errorId, PDO::PARAM_INT);
        $stmt->execute();
   
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        foreach ($stmt->fetchAll() as $listRow) {
            $error = new Listing($conn, $listRow);
        }    // close out foreach loop
      
        return $error;
    } // close out static getErrorById function    
        
    //updates the error in the db when the user selects
    function updateError() {
        $update = "UPDATE errorLog SET
            errorNotes=:errorNotes,
            resolved=:resolved,
            whatNeedsDone=:whatNeedsDone,
            responsibleParty=:responsibleParty,
            primaryImage=:primaryImage,
            imageTitle=:imageTitle
            WHERE errorId=:errorId";
        
        $stmt = $this->conn->prepare($update);
        $stmt->bindParam(':errorId', $this->errorId, PDO::PARAM_INT);
        $stmt->bindParam(':errorNotes', $this->errorNotes);
        $stmt->bindParam(':resolved', $this->resolved, PDO::PARAM_BOOL);
        $stmt->bindParam(':whatNeedsDone', $this->whatNeedsDone);
        $stmt->bindParam(':responsibleParty', $this->responsibleParty);
        $stmt->bindParam(':primaryImage', $this->primaryImage);
        $stmt->bindParam(':imageTitle', $this->imageTitle);
        $stmt->execute();
    }// closes out updateError Function

    //creates new Error in the database
    function createError() {
        $insert = "INSERT INTO errorLog
            (errorNotes, whatNeedsDone, responsibleParty, reportedDate, resolved, primaryImage, imageTitle)
            VALUES
            (:errorNotes, :whatNeedsDone, :responsibleParty, :reportedDate, :resolved,  :primaryImage, :imageTitle)";
        
        $stmt = $this->conn->prepare($insert);
        $stmt->bindParam(':errorNotes', $this->errorNotes);
        $stmt->bindParam(':whatNeedsDone', $this->whatNeedsDone);
        $stmt->bindParam(':responsibleParty', $this->responsibleParty, PDO::PARAM_INT);
        $stmt->bindParam(':reportedDate', $this->reportedDate);
        $stmt->bindParam(':resolved', $this->resolved, PDO::PARAM_BOOL);
        $stmt->bindParam(':primaryImage', $this->primaryImage);
        $stmt->bindParam(':imageTitle', $this->imageTitle);
        $stmt->execute();
    } // closes out createError function
    
    // delete error from database
    function deleteError() {
        $delete = "DELETE FROM errorLog WHERE errorId=:errorId";
        $stmt = $this->conn->prepare($delete);
        $stmt->bindParam(':errorId', $this->errorId, PDO::PARAM_INT);
        $stmt->execute();
    } // closes out deleteError function
    
}// closes out listing class