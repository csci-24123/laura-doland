DROP DATABASE IF EXISTS finalProjectLauraDoland;
CREATE DATABASE finalProjectLauraDoland;
use finalProjectLauraDoland;

DROP TABLE IF EXISTS users;
CREATE TABLE users(
	userId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    userFullName VARCHAR (1000),
    userLogin VARCHAR (1000),
    userPassword VARCHAR (1000)
);
    
DROP TABLE IF EXISTS errorLog;
CREATE TABLE errorLog (
    errorId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    errorNotes VARCHAR (1000),
    whatNeedsDone VARCHAR (1000),
    responsibleParty INT,
    reportedDate DATE,
    resolved bool,
    primaryImage mediumblob,
    imageTitle blob,
    FOREIGN KEY (responsibleParty) REFERENCES users(userId) ON DELETE CASCADE
);

INSERT INTO users(userFullName, userLogin, userPassword)
VALUES
("Laura Doland", "l.d@us.ibm.com", 123),
("Stephanie Erlenbach", "s.e@us.ibm.com", 234),
("Raenel Bryan", "r.b@us.ibm.com", 345),
("Rob Ridlehoover", "r.r@ecom.com", 456),
("Brian Doland", "b.d@ecom.com", 567),
("Alex Doland", "a.d@ecom.com", 678); 

INSERT INTO errorLog (errorNotes, whatNeedsDone, responsibleParty, reportedDate, resolved)
VALUES
("Receiver ID ZZ/ABC is not setup.", "3/16/2022 - Laura Doland - Ecom provided GXS as the VAN. Laura added the ID and asked Stephanie if ABC Company wanted the data restarted. 3/15/2022 - Laura Doland - Ecom need to provide the VAN ZZ/ABC uses.", 1, "2022-03-15", 1),
("Sender ID ZZ/ABC is not setup to send an INVOIC", "3/15/2022 - Stephanie Erlenbach -  IBM needs to add Edifact to the list and resend the data", 1, "2022-03-15", 1),
("ZZ/DOLANDTEST is sending both a 0a and 0d for the segment terminator.", "3/16/2022 - Rob Ridlehoover - Ecom told to correct with one terminator and resend", 2, "2022-03-15", 0),
("ZZ/ROBTEST is sending 4010 in the GS08", "3/16/2022 - Laura Doland - Ecom needs to change the value to 004010 and resend", 3, "2022-03-15", 0),
("ZZ/ABC is sending an 850 that is not showing up at IBM", "3/16/2022 - Rob Ridlehoover - Test Company told to contact GXS for mailbag number", 4, "2022-03-16", 0),
("ZZ/DEF is sending both a ~ and a hex 25", "3/17/2022 - Laura Doland -  Ecom asked to reove the hex 25 and resend the data", 2, "2022-03-17", 0);
