<?php
include("../includes/navbar.php");
$imageErr = ""; // needed for uploads

// clean the data
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $errorNotes = clean_input($_POST["errorNotes"]);
    $whatNeedsDone = clean_input($_POST["whatNeedsDone"]);
    $primaryImage = file_get_contents($_FILES['fileToUpload']['tmp_name']);  
    $imageTitle = htmlspecialchars($_FILES["fileToUpload"]["name"]);

    //check if marked resolved or not
    $resolved = "0";
    if (isset($_POST['resolve'])) {
        $resolved = "1";
    } // closes out resolve if
    
    // assign data to variables for new error
    if (!empty($errorNotes) && !empty($whatNeedsDone)) {
        $responsibleParty = getUserId($_SESSION['userLogin']); 
        $reportedDate = date('Y-m-d');
   
        $errorInfo = array(
            "errorId" => "",
            "reportedDate" => $reportedDate,
            "resolved" => $resolved,
            "errorNotes" => $errorNotes,
            "whatNeedsDone" => $whatNeedsDone,
            "responsibleParty" => $responsibleParty,
            "primaryImage" => $primaryImage,
            "imageTitle" => $imageTitle
        ); // closes out errorInfo array
        
        $error = new Listing($conn, $errorInfo);
        $error->createError();

        header("Location: fullErrors.php");
    }  // closes out content if
} // closes out Post if
?>

<h1 class = display-4>Fill out the form below to create a new error.</br></h1>
</br>

<!--create form for new error-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 col-lg-8 col-xl-7">
            <form enctype="multipart/form-data" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">              
                         
                <div class="form-group">
                    <label for="errorNotes">Error Notes</label>
                    <span class="error">*<br>
                    <input type="text" class="form-control" name="errorNotes" id="errorNotes" 
                    required>
                </div> <!--closes out div for error notes -->

</br>
                <div class="form-group">
                    <label for="fileToUpload">Select image to upload:</label>
                    <input type="file" name="fileToUpload" id="fileToUpload" required>
                    <span class="error">* <?php echo $imageErr;?></span><br>
                </div> <!--closes out div for fileToUpload -->
</br>
                <div class="form-group">
                    <label for="whatNeedsDone">What needs to be done:</label>
                    <span class="error">*</span><br>
                    <textarea rows="10" class="form-control" name="whatNeedsDone" id="whatNeedsDone" required></textarea>
                </div> <!-- closes out content div -->

                <div class="form-group">
                    <label for="resolved">Is the Error resolved?</label>
                    <input type="checkbox" id="resolved" name="resolved">
                </div> <!-- closes out checkbox div -->
        
</br>
                <input type="submit" class="buttonCreate" value="Submit">
            </form> <!--closes out form -->    
        </div> <!-- closes out third div -->
    </div> <!-- closes out second div -->
</div> <!-- closes out first div -->
