<?php
include("../includes/navbar.php");


$userFullName = $userlogin = $userpassword = "";
$passwordErr = $usernameErr = $companyNameErr = "";
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userFullName = clean_input($_POST["userFullName"]);
    $userLogin = clean_input($_POST["userLogin"]);
    $password1 = clean_input($_POST["password1"]);
    $password2 = clean_input($_POST["password2"]);
 
    if ($password1 !== $password2) {
      $userPassord = "";
      $passwordErr = "Passwords must match";
    } else {
      $userPassword = password_hash($password1, PASSWORD_DEFAULT);
    }
 
    $usernameErr = checkUsernameIsUnique($userLogin);

    if (empty($passwordErr)) {
      addUser($userFullName, $userLogin, $userPassword);
      login($userLogin);
    }
}
 
function checkUsernameIsUnique($userLogin) {
    $conn = connect_to_db("finalprojectlauradoland");
    $selectUser = "SELECT userLogin FROM users WHERE userLogin =:userLogin";
    $stmt = $conn->prepare($selectUser);
    $stmt->bindParam(':userLogin', $userLogin);
    $stmt->execute();
   
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return empty($stmt->fetchAll()) ? "" : "Username is already taken";
  }
  
function addUser($userFullName, $userLogin, $userPassword) {
    $conn = connect_to_db("finalprojectlauradoland");
    $insert = "INSERT INTO users (userFullName, userLogin, userPassword)
    VALUES (:userFullName, :userLogin, :userPassword)";
    $stmt = $conn->prepare($insert);
    $stmt->bindParam(':userFullName', $userFullName);
    $stmt->bindParam(':userLogin', $userLogin);
    $stmt->bindParam(':userPassword', $userPassword);
    $stmt->execute();
}

?>

<style>
    .error {color: #FF0000;}
</style>
<h1 class = display-4>Create a new user using the form below</h1>
</br>
<div class='userLoginForm container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="form-group">
                    <label for="userFullName">Full Name</label>
                    <input type="text" class="form-control" name="userFullName" id="userFullName">
                </div>
                <div class="form-group">
                    <label for="userLogin">Username</label>
                    <span class="error">* <?php echo $usernameErr;?></span><br>
                    <input type="text" class="form-control" name="userLogin" id="userLogin" required>
                </div>
                <div class="form-group">
                    <label for="password1">Password</label>
                    <span class="error">* <?php echo $passwordErr;?></span><br>
                    <input type="password" class="form-control" name="password1" id="password1" required>
                </div>
                <div class="form-group">
                    <label for="password2">Repeat Password</label>
                    <span class="error">* <?php echo $passwordErr;?></span><br>
                    <input type="password" class="form-control" name="password2" id="password2" required>
                </div>  
                </br>
                <input type="submit" class="buttonCreate" value="Submit">
            </form>
        </div>
    </div>
</div>

