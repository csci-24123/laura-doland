<?php
include("../includes/navbar.php");

$error = Listing::getErrorById($conn, $_GET['errorId']);

if (isset($_GET['errorId'])) {
    try {
        $error = Listing::getErrorById($conn, $_GET['errorId']);
    } catch(Exception) {
        header("Location: 404.php");
    }
} else {
    header("Location: 404.php");
}
?>

<header class="masthead"
    <?php if (!empty($article->primaryImage)) { ?>
        style="background-image:url(data:image/jpeg;base64,<?php echo base64_encode( $article->primaryImage )?>)"
    <?php
        }
    ?>
>

<div class="container position-relative px-4 px-lg-5">
    <div class="row gx-4 gx-lg-5 justify-content-center ">
        <div class="col-md-10 col-lg-8 col-xl-7">
            <div class="post-heading">
                <h1><?php echo $error->errorNotes; ?></h1>
                <span class="meta">
                    <?php echo "<br>"; ?>
                    <h4> <?php echo ("What needs done:<br>");?></h4>
                    <p><?php echo ($error->whatNeedsDone);?></p>
                    <?php echo "<br>";?>
                    <h4><?php echo ("What date was the error reported:<br>")?></h4>
                    <p><?php echo ($error->reportedDate); ?></p>
                    <?php echo "<br>"; ?>
                    <h4> <?php echo ("What is the status of the error:<br>") ?> </h4>
                    <p><?php echo ($error->resolved ? "Resolved" : "Not Resolved");?> </p>   
                    <?php echo "<br>"; ?> 
                </span>
                <h4>Failing data:</h4>
                    <img class="d-block w-100" alt="<?php echo $error->imageTitle;?>" src='data:image/jpeg;base64,<?php echo base64_encode( $error->primaryImage )?>' />
            </div>
        </div>
    </div>
</div>
</header>