<?php
include("../includes/navbar.php");
 
$userLogin = $userPassword = "";
$passwordErr = $usernameErr = "";
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $userLogin = clean_input($_POST["userLogin"]);
  $userPassword = clean_input($_POST["userPassword"]);
 
  if (!empty($userLogin) && !empty($userPassword)) {
    $usernameErr = verifyUser($userLogin);
    $passwordErr = verifyPassword($userLogin, $userPassword);
    
    if (empty($usernameErr) && empty($passwordErr)) {
        login($userLogin);
      }
    }
  
  }
  
function verifyPassword($userLogin, $userPassword) {
    $conn = connect_to_db("finalprojectlauradoland");
    $selectUser = "SELECT userLogin, userPassword FROM users WHERE userLogin=:userLogin";
    $stmt = $conn->prepare($selectUser);
    $stmt->bindParam(':userLogin', $userLogin);
    $stmt->execute();
   
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
        $hashedPassword = $listRow['userPassword'];
        if (!password_verify($userPassword, $hashedPassword)) {
            return "Incorrect password";
        }
      
    }
  }
  
function verifyUser($userLogin) {
    $conn = connect_to_db("finalprojectlauradoland");
    $selectUser = "SELECT userLogin FROM users WHERE userLogin=:userLogin";
    $stmt = $conn->prepare($selectUser);
    $stmt->bindParam(':userLogin', $userLogin);
    $stmt->execute();
   
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return empty($stmt->fetchAll()) ? "Username does not exist" : "";
}

?>


<style>
    .error {color: #FF0000;}
</style>
<div class='userLoginForm container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="form-group">
                    <label for="userLogin">Username</label>
                    <span class="error">* <?php echo $usernameErr;?></span><br>
                    <input type="text" class="form-control" name="userLogin" id="userLogin" required>
                </div>
                <div class="form-group">
                    <label for="password1">Password</label>
                    <span class="error">* <?php echo $passwordErr;?></span><br>
                    <input type="password" class="form-control" name="userPassword" id="userPassword" required>
                </div>
                <input type="submit" class="buttonCreate" value="Submit">
            </form>
        </div>
    </div>
</div>