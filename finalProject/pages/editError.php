<?php
include("../includes/navbar.php");

//choose error Id
if (isset($_GET['editErrorId'])) {
    try {
        $error = Listing::getErrorById($conn, $_GET['editErrorId']);
    } catch(Exception) {
        header("Location: fullErrors.php");
    }
} else {
    header("Location: fullErrors.php");
}//closes out edit Error ID if

$imageError = "";

// check if image is too big
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $mbFileSize = $_FILES["fileToUpload"]["size"] / 1000000;
    if ($mbFileSize > 10) {
        $imageError = "Your file is too large. Max file size is 10MB. Yours was $mbFileSize MB";
    }
  
    // clean the input
    $errorNotes = clean_input($_POST["errorNotes"]);
    $whatNeedsDone = clean_input($_POST["whatNeedsDone"]);
    $primaryImage = file_get_contents($_FILES["fileToUpload"]["tmp_name"]);
    $imageTitle = htmlspecialchars($_FILES["fileToUpload"]["name"]);

    // check if issue is resolved
    $resolved = "0";
    if (isset($_POST['resolved'])) {
      $resolved = "1";
    }
   
    // set vaariable if empty
    if (!empty($errorNotes) && !empty($whatNeedsDone)) {
        $error->errorNotes = $errorNotes;
        $error->whatNeedsDone = $whatNeedsDone;
        $error->resolved = $resolved;
        $error->primaryImage = $primaryImage;
        $error->imageTitle = $imageTitle;
        $error->updateError();
        header("Location: fullErrors.php");
    }
}
  
?>

<!--form pulling in the data and allowing edits-->

<h1 class = display-4>Update the form below</h1> 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 col-lg-8 col-xl-7">
            <form enctype="multipart/form-data" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    
                <div class="form-group">
                    <label for="errorNotes">What is the Error?</label>
                    <span class="error">*<br>
                    <input type="text" class="form-control" name="errorNotes" id="errorNotes" value="<?php echo $error->errorNotes ?>" required>
                </div><!--closes form-group div-->
                
                <div class="form-group">
                    <label for="whatNeedsDone">Notes for what needs done / is being done to resolve:</label>
                    <span class="error">*</span><br>
                    <textarea rows="10" class="form-control" name="whatNeedsDone" id="whatNeedsDone" required><?php echo $error->whatNeedsDone ?> </textarea>
                </div> <!--closes form-group div-->
        
                <div class="row">
                    <div class="col-9 col-md-6">
                        <div class="form-group">
                            <label for="fileToUpload">Select image to upload:</label>
                            <input type="file" name="fileToUpload" id="fileToUpload">
                            <span class="error">*  <?php echo $imageError;?></span><br> 
                        </div><!--closes form-group div-->
                    </div> <!--closes col-9 div-->
            
                    <div class="col-3 col-md-6">
                        <?php if (!empty($error->primaryImage)) { ?>
                            <img class="d-block w-100" alt="<?php echo $error->imageTitle;?>" src='data:image/jpeg;base64,<?php echo base64_encode( $error->primaryImage )?>' />
                        <?php } ?>
                    </div><!--closes col-3 div-->
                </div> <!--closes row div-->

                <div class="form-group">
                    <label for="resolved">Issue is Resolved:</label>
                    <input type="checkbox" id="resolved" name="resolved" <?php echo ($error->resolved ? "checked" : "") ?>>
                </div> <!--closes form-group div-->
        
            <input type="submit" class="buttonCreate" value="Submit">
            </form>    
        </div> <!--closes col-md-10 div-->
    </div> <!--closes row-justify div-->
</div> <!--closes container div-->