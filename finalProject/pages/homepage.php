<html>
    <head>
        <!-- Adds the page title -->
        <title>SCBN Migration Issues</title> 
    
    </head>
    <body>
        <div>
            <?php
                // link to the navbar
                include("../includes/navbar.php"); 
            ?>
 
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                
                <!--Header for the page-->
                <h1 class = "display-4">Welcome to the SCBN Migration Issue Log</h1>

                <!-- Header for notes about non-printable characters -->    
                <h2>Special notes about non-Printable Characters!!</h2>

                    <!-- Unordered list of notes -->
                    <ul>
                        <li>As a best practice, we highly recommend only using a single printable character such as the tilde (~) as a segment terminator.</li>
                        <li>The following Hex values are often problematic and may cause data loss on SCBN:</li>
                        <ul>
                            <li>EBCDIC Hex Values: 0A, 0D, 15, 25</li>            
                            <li>ASCII Hex Values: 10, 13</li>
                        </ul>
                        <li>The following Hex values will result in data failure on SCBN:</li>
                        <ul>
                            <li>EBCDIC Hex Values: 00, 02, 03, 13, 26, 1D, 1E, 1F, 3F</li>         
                            <li>ASCII Hex Values:00, 02, 03, 13, 17, 1A, 1D, 1E, 1F</li>
                        </ul>
                        <li>Data sent with multiple segment terminators will fail on SCBN.</li>
                    </ul>
                
                <!-- Header for typical errors and issues -->
                <h2>Typical errors and issues that come up when sending data to SCBN</h2>
                    
                    <!-- Unordered list of errors and issues -->                
                    <ul>
                        <li class = "recIdNotRecognized">0223 RECEIVER ID NOT RECOGNIZED, INTERCHANGE DELIVERED TO CUST SERVICE</li>
                        <ul>
                            <li>Verify with your trading partner that you have the correct ID and Qualifier.</li>
                            <li>Verify that the receiver Qualifier and ID is correct in your translation software.</li>
                            <li>Add the ID on Customer Center (link below)</li> 
                        </ul>
                        <li class = "gsMissing"> 0224 GROUP START SEGMENT MISSING, SENDING INTERCHANGE TO CUST SERVICE</li>
                        <ul>
                            <li>Verify you have a GS segment?</li>
                            <li>Are you using a valid segment terminator?</li>
                            <li>Is your ISA 106 characters long?</li>
                            <li>Are you using more than one segment terminator?</li>
                            <li>Is your GS08  correctly formatted.  ie: 004010 or 004010UCS?</li>
                        </ul>
                        <li class = "senIdNotRecognized">Error Code: 0205 SENDER ID CANNOT BE IDENTIFIED, SENDING INTERCHANGE TO CUST SERVICE</li>
                        <ul>
                            <li>Verify that the sender Qualifier and ID is correct in your translation software.</li>
                            <ul>
                                <li>If correct add Qualifier and ID in Customer Center.</li>
                                <li>If incorrect make changes and resend.</li>
                            </ul>
                        </ul>
                        <li class = "senderNotSetup">Error Code: 0211 SENDER IS NOT SET UP TO SEND FORMAT, SENDING INTERCHANGE TO CUST SERVICE</li>
                        <ul>
                            <li>The interchange contains a sender ID that is not set up to send the document type within that interchange.</li>
                            <ul>
                                <li>Call Customer Support to have the document type added.</li>
                            </ul>
                        </ul>
                    </ul>
                
                <!-- Link to Customer Center -->    
                <a class = "customerCenter" href = "https://login.ibm.com/authsvc/mtfim/sps/authsvc?PolicyId=urn:ibm:security:authentication:asf:basicldapuser">Visit Customer Center to view your data and open tickets with support.</a>
            </div>
        </div>
    </div>       
</body>
</html>