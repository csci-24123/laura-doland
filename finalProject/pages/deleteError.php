<?php
include("../includes/navbar.php");
$errorId = $_GET['deleteErrorId'];

// delete error if selected
if (isset($errorId)) {
    try {
        $error = Listing::getErrorById($conn,  $_GET['deleteErrorId']);
    } catch(Exception) {
        header("Location: fullErrors.php");
    }
} else {
    header("Location: fullErrors.php");
} // closes out errorId if
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $error->deleteError();
    header("Location: fullErrors.php");
}// closes post if
?>

<!--creates buttons to delete or cancel-->
<div class="container">
    <div class="row justify-content-center text-center">
        <div class="col-md-10 col-lg-8">
            <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <label for="submit">Are you sure you want to delete this error?</label>
                <br> <br>
                <input type="submit" class="buttonCreate" value="Delete">
            </form>
        </div> <!--closes col-md-10 div-->
        
        <div class="col-md-10 col-lg-8">
          <p>If not please cancel.</p>
          <a href="fullErrors.php" class="buttonDelete">Cancel</a>
        </div><!--closes col-md-10 div-->
    </div> <!--closes row  justify div-->
</div><!--closes container div-->