<?php
include("../includes/navbar.php");
$errors = Listing::getErrorsFromDb($conn);

if (!isset($_SESSION['userLogin'])) {
    header("Location: 404.php");
}
?>

<h1 class = display-4>Below is the list of errors that have been identified.</h1>
<div class="container">  
    <div class="row">
        <div class="d-flex justify-content-center">
            <a class='buttonCreate' href='createError.php'>Create New Error</a>
        </div>
    </div>
<?php
    foreach ($errors as $error) {
	      $errorId = $error->errorId;
?>
        <div class="row">
          <div class="col-12 listing-wrapper">
              <div class="row">
                  <div class = "col-12 col-md-2">  
                      <p><?php echo ($error->resolved ? "Resolved" : "Not Resolved");?> </p>
                  </div>

                  <div class="col-12 col-md-6">
                      <a class="" href=	"errorlogpage.php?errorId=<?php echo $errorId ?>">
                          <span><?php echo $error->errorNotes ?></span>
                      </a>
                  </div>
                  <div class="col-12 col-md-4 text-end">
                    <a class='buttonEdit' href='editError.php?editErrorId=<?php echo $errorId ?>'>Edit</a>
                    <a class='buttonDelete' href='deleteError.php?deleteErrorId=<?php echo $errorId ?>'>Delete</a>
                  </div>
              </div>
        </div>
    </div>
  <?php
    }
  ?>
</div>
