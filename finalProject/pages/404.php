<?php
include("../includes/navbar.php");
?>
<html>
    <head>
        <title>404 Page </title>
    </head>
    <body>
        <header class="masthead">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="post-heading">
                            <h1>404 - Page Not Found</h1>
                            <span class="meta">
                                Sorry, it looks like the page you were looking for does not exist.
                                Feel free to go back to the <a class = homepage href="homepage.php">homepage</a>
                            </span>
                        </div>  <!--closes post-heading div-->                        
                    </div> <!--closes col-md-10 div-->
                </div><!--closes row gx-4 div-->
            </div> <!--closes container postion div-->
        </header>
    </body>
</html>