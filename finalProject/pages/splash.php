<?php
  include("../includes/navbar.php");
  if (isset($_SESSION['userLogin'])) {
?>
 
<div class='container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <h1>Hello <?php echo $_SESSION['userLogin'] ?></h1>
            <h5></br>Welcome To The IBM Migration Error Log for XYZ Company</h5>
            <ul>
  </br>
                <li>Click the button below to:</li>
                <ul>
                    <li>Create new errors</li>
                    <li>Look at the status of current errors</li>
                    <li>Add notes to your errors</li>
                    <li>Mark errors resolved </br></br></li>
            <a class="buttonCreate" href="../pages/fullErrors.php">View/Edit Errors</a>
        </div>
    </div>
</div>


<?php
} else {
    header('Location: homepage.php');  
}
?>
