# Lab 1 - Laura Doland
This lab is to help me install git.  Begin to use git. Walk me through git workflows. Help me create a git repository to use in this class. 
## Big git no-no
Never commit directly to the main branch in a repo
## Merge Conflicts
We actually want it to say this!
## About Me
I am married and have a 25 year old daughter.  I have my bachelor's degree in Supply Chain Management and have been assisting companies in doing EDI for IBM for 23 years.  I have wanted to take a few classes to update my skills for a few years so when the pandemic hit, my husband was working evenings, I decided to start taking classes.  It has been very helpful to my career leading to a promotion and nice pay raise.  With another one likely this year.  I am a huge Blue Jackets fan.  I have a fun part-time job working parking for events at OSU and Nationwide Arena.  I can get into any event for free on campus which is a great perk.  An interesting fact we adopted our daughter when she was 7.  It has been quite the journey.
## Proposed Final Project
I would like to build a website that I can use for my job and I can demonstrate to my manager.  While it won't be able to be used it's what we need to be built for us.  It will prove the knowledge I have gained through this class.  A big part of what my department does is verify information with customers.  The first page that is visible without signing in will be a letter from the sponsoring company explaining what is going on.  Once they login they will be presented with information that they need to verify and questions that they need to answer.  Challenges I see I hope I can have a database of information that when the user logs in it prepopulates for them to verify.  Also what happens if a different user logs in then expected.  This website would be for a select audience of trading partners of the sponsor company.  It would be important for them to access to verify we have the correct information and there is no supply chain interruption.


