<?php
$starWarsFilms = array("A New Hope", "The Empire Strikes Back", "Return of the Jedi");
print_r($starWarsFilms);
echo $starWarsFilms[1]; // it's the second element of the array, so it has an idex of 1.  Trust me you'll get used to it.
echo count($starWarsFilms); // outputs 3
sort ($starWarsFilms); // the order of the array is not "A New Hope", "Return of the Jedi", "The Empire Strikes Back" because it sorts alphabetically
array_push($starWarsFilms, "The Phantom Meance");
unset ($starWarsFilms[0]); // removes "A New Hope"

$favoriteSportsTeams = array("Columbus Blue Jackets", "Columbus Crew", "Ohio State Buckeyes", "Florida Gators", "Notre Dame Fighting Irish");

$releaseDate = array("A New Hope" => 1977, "Empire Strikes Back" => 1980, "Return of the Jedi" => 1983);
echo "Empire was released in " . $releaseDate["Empire Strikes Back"] . "</br>";

$documentTypes = array("Purchase Order" => 850, "Invoice" => 810, "Advanced Ship Notice" => 856);
echo "A purchase order is known as an " . $documentTypes["Purchase Order"] . "</br>";

$i = 0;
while ($i <= 5) {
    echo "The current number is $i </br>";
    $i++;
}
for ($i = 0; $i <= 5; $i++) {
    echo "The current number is $i </br>";
}
for ($i = -52; $i <= 100; $i += 5) {
    echo "The current number is $i </br>";
}

for ($i = 1; $i <= 20; $i++) {
    echo "$i sheep jumping the moon. </br>";
}

$avengers = array("Thor", "Captain America", "Iron Man", "Black Widow");

foreach ($avengers as $hero) {
    echo "$hero is in the Avengers </br>";
}

foreach ($favoriteSportsTeams as $team) {
    echo "$team is one of my Favorite Teams. </br>";
}

function echoHelloWorld() {
    echo "Hello World </br>";
}
echoHelloWorld(); //prints hello world
echoHelloWorld(); //prints hello world again

function echoName($name) {
    echo "Hello $name</br>";
}
echoName("Jackie"); // prints Hello Jackie
echoName("Robinson"); // prints Hello Robinson


function checkDivisibleBy3or5($numberToTest) {
    if ($numberToTest %3 == 0 && $numberToTest %5 == 0) {
        echo "$numberToTest is divisible by 3 and 5 </br>";
    } elseif ($numberToTest %5 === 0) {
        echo "$numberToTest is divisible by 5 </br>";
    } elseif ($numberToTest %3 === 0) {
        echo "$numberToTest is divisible by 3 </br>";
    } else {
        echo "$numberToTest is not divisible by 3 or 5 </br>";
    }
}
checkDivisibleBy3or5(3);
checkDivisibleBy3or5(5);
checkDivisibleBy3or5(9);
checkDivisibleBy3or5(10);
checkDivisibleBy3or5(15);
checkDivisibleBy3or5(19);
    
// this function has 2 inputs
function loginUser($userName, $password) {
    //if $userName and $password match a user from the database, log them in
}

loginUser("AzureDiamond", "hunter2");

function cube ($numToCube) {
    $cube = $numToCube * $numToCube * $numToCube;
    return $cube;
}
$cubeOf3 = cube(3);
$cubeOf5 = cube(5);
echo "$cubeOf3 </br>"; // prints 27
echo "$cubeOf5 </br>"; // prints 125
echo $cubeOf3 * $cubeOf5 . "</br>"; // prints 3375

function divide($dividend, $divisor) {
    if ($divisor == 0) {
        throw new Exception ("Division by zero");
    }
    return $dividend / $divisor;
}
try {
    echo divide(5, 0);
}
catch (Exception $ex) {
    $message = $ex->getMessage();
    $file = $ex->getFile();
    $line = $ex->getTraceAsString();
    echo "$message $file $line";
} 
/*finally {
    echo "Process Complete.";
}*/
