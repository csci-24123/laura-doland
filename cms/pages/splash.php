<?php
  include("../includes/navbar.php"); 
  if (isset($_SESSION['username'])) {
?>
 
<div class='container'>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
            <h1>Hello <?php echo $_SESSION['username'] ?></h1>
        </div>  <!-- closes out div class col-12 -->
    </div> <!-- closes out div class row -->
</div> <!-- closes out div class container -->
<div class = "col-12 listing-wrapper">
    <div class = "row">
        <div class = "col-12 col-md-7">
        <a class='btn btn-success' href="ArticleListing.php">Edit / View Articles</a>
        </div> <!-- coses out div col-md -->
    </div> <!-- closes out div row-->
</div> <!-- closses out div wrapper 1 -->

 
<?php
} else {
?>
   
<a href="login.php">Please log in to see this page</a>
 
<?php
} // closes out else statement
?>