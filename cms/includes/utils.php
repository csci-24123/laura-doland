<?php
function clean_input($data) {
    $data = trim($data); // removes whitespace
    $data = stripslashes($data); // strips slashes
    $data = htmlspecialchars($data); // replaces html chars
    return $data;
} // closes out clean input function
 
function connect_to_db($dbName) {
    $servername = "localhost";
    $username = "root";
    $password = "";
    try {
        return new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);      
    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
} // closes out connect_to_db function

function getUserId($conn, $username) {
    $select = "SELECT userId from users WHERE username=:username";
    $stmt = $conn->prepare($select);
    $stmt->bindParam(':username', $username);
    $stmt->execute();
 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach($stmt->fetchAll() as $listRow) {
        return $listRow['userId'];
    } // closes out foreach loop
} // closes out getUserId function
