<div class="container">
    <div class="row">
        <?php
            $data = Article::getArticlesFromDb($conn, 9);
            foreach ($data as $article) {
        ?>
                <div class="col-4">
                    <a class="card-wrapper"href="../pages/articlePage.php?articleId=<?php echo $article->articleId ?>">
                        <div class="card">
                            <?php if (!empty($article->primaryImage)) { ?>
                                <img alt="<?php echo $article->imageTitle;?>" src='data:image/jpeg;base64,<?php echo base64_encode( $article->primaryImage )?>' />
                            <?php } ?>
                            <h2><?php echo $article->title ?></h2>
                            <p><?php echo $article->publishDate ?></p>
                        </div> <!--closes out div card-->
                    </a> <!--closes out a-->
                </div> <!--closes out div col-4-->
            <?php
            } //closes out foreach loop
            ?>
        </div> <!--closes out div row-->
</div> <!--closes out div container-->