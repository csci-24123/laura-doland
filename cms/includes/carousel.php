<?php
$numSlides = 5;
 
$articles = Article::getArticlesFromDb($conn, $numSlides, true);
$numSlides = count($articles) < $numSlides ? count($articles) : $numSlides;

if ($numSlides > 0) {
    ?> <!--close out opening php-->
     
    <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <?php // switch to php for for statement
                    for ($i=1; $i<$numSlides; $i++) {
                        ?> <!--closing php for for statement -->
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="<?php echo $i; ?>" aria-label="Slide <?php echo $i + 1; ?>"></button>
                        <?php  // switch to php to close out for statement
                    } //closes out for loop
                        ?> <!--closes out php for bracket -->
        </div> <!-- div tag closes out carousel-indicators -->
        <div class="carousel-inner">
            <?php // switch to php for foreach statement
                foreach ($articles as $index => $article) {
                    ?> <!-- closes out foreach php -->
                    <div class="carousel-item <?php echo ($index == 0 ? 'active"' : '"'); ?> data-bs-interval="10000">
                        <a href="articlePage.php?articleId=<?php echo $article->articleId ?>">  
                            <?php if (!empty($article->primaryImage)) { ?>
                                <img class = "d-block w-100" alt="<?php echo $article->imageTitle;?>" src='data:image/jpeg;base64,<?php echo base64_encode( $article->primaryImage )?>' />
                            <?php } else { ?>     
                                <svg class="bd-placeholder-img bd-placeholder-img-lg d-block w-100" width="800" height="400" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#777"></rect></svg>
                            <?php } ?>
                            <div class="carousel-caption d-xs-block">
                                <h3><?php echo $article->title; ?></h3>
                                <p><?php echo $article->author; ?></p>
                            </div> <!-- closes out div carousel caption -->
                        </a>
                    </div> <!-- closes out div for carousel item -->
                    <?php // switch to php to close out foreach loop
                } // closes foreach loop
                ?><!--closes out the php for the braket -->
        </div><!-- div that closes out carousel inner -->
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button> <!-- closes out button carousel-control-prev -->
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button> <!-- closes out button carousel-control-next -->
  
        </div> <!-- div tag closes out carouselExampleDark -->  
    <?php // switch to php to close out if statement
} // closes out if statement with $numSlides
    ?> <!-- closes out php for } -->