DROP DATABASE IF EXISTS cms;
CREATE DATABASE cms;
use cms;
 
DROP TABLE IF EXISTS roles;
CREATE TABLE roles (
    roleId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    roleName VARCHAR(255),
    canEdit BOOL,
    canAuthor BOOL,
    canAlter BOOL
);
 
DROP TABLE IF EXISTS users;
CREATE TABLE users (
    userId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    username VARCHAR(255),
    userPassword VARCHAR(255),
    fullName VARCHAR(255),
    siteRole INT,
    FOREIGN KEY (siteRole) REFERENCES roles(roleId) ON DELETE CASCADE
);
 
DROP TABLE IF EXISTS articles;
CREATE TABLE articles (
    articleId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(255),
    authorId INT,
    content VARCHAR(10000),
    publishDate DATE,
    isPublished BOOL,
    FOREIGN KEY (authorId) REFERENCES users(userId) ON DELETE CASCADE
);
 
INSERT INTO roles (roleName, canEdit, canAuthor, canAlter)
VALUES
("admin", 1, 1, 1),
("author", 1, 1, 0),
("editor", 1, 0, 0);
 
INSERT INTO users (username, fullName, siteRole)
VALUES
("robinclo", "Robin Clower", 1),
("darnellkin", "Darnell King", 2),
("slavanov", "Slava Novik", 3);
 
INSERT INTO articles (title, authorId, content, publishDate, isPublished)
VALUES
("Lorem ipsum dolor sit", 1, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "2021-10-31", 1),
("Sit dolor ipsum lorem", 2, "Adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "2022-03-31", 0),
("Dolar ipsum lorem", 2, "Incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "2022-02-12", 1);
 
SELECT * from roles;
SELECT * from users;
SELECT * from articles;

SELECT roles.roleName, users.*
FROM roles
LEFT JOIN users ON users.userId=roles.roleId
WHERE roles.roleId = 1;

SELECT u.userId, u.fullName, a.title, a.publishDate
FROM users AS u
LEFT JOIN articles AS a ON u.userId=a.authorId
WHERE u.userId=1;

SELECT u.username, r.roleName
FROM users AS u
INNER JOIN roles AS r ON r.roleId=u.siteRole;

ALTER TABLE articles ADD primaryImage MEDIUMBLOB;
ALTER TABLE articles ADD imageTitle VARCHAR(255);