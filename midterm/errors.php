<?php
/* 
There are at least 8 things that should be changed about this code. 
Some of them will prevent the program from running,
others are stylistic/good coding practices.
The output you should see in your browser is 
"Ludwig van Beethoven has a famous song called Ode to Joy"
Hint: Double check that your if statement works by changing the value for $x
*/ 
 
$musician = "Ludwig van Beethoven";
$song = "Ode to Joy";
 
if ($musician == "Ludwig van Beethoven") {
    echo "$musician has a famous song called $song";
}
?>