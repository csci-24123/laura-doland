<html>
<header>
    <title>Movie Reviews</title>
</header>

<?php
    include("../cms/includes/navbar.php");
    $conn = connect_to_db("midtermlaura");
?>

<body>
    <h1>Welcome To Your Movie Reviews.</h1>
<?php

//variables needed for the form

$reviewText = "";
$reviewErr = "";
$numStars = "";
$numStarsErr = "";

//connect to database to post inputed data

if ($_SERVER["REQUEST_METHOD"] == "POST"){
           
    if (empty($_POST["reviewText"])) {
        $reviewErr = "Review is required";
    } else {
        $reviewText = clean_input($_POST["reviewText"]);
    }
            
    if (empty($_POST["numStars"])) {
        $numStarsErr = "The number of stars is required";
    } else {
        $numStars = clean_input($_POST["numStars"]);
    }
}
?>
<!-- style for the required field error -->
<style>
    .error {color:#FF0000;}
</style>

<!-- instruct the user that a field is required -->
<p><span class = "error">* required field </span></p> 
        
<!-- build the form -->
<div class='review'>
<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
   
    <fieldset class = "form-check">
 
        <label for="reviewText">Please write a Review of your movie:<span class = "error">*</span></label>
        <input type="text" name="reviewText" id="reviewText" required><br>
    </fieldset>    
    <fieldset class = "form-check">
        <legend>Please rate the movie you reviewed: <span class = "error">*</span></legend>
    
        <input class = "form-check-input" id = "1" type = "radio" name = "numStars" value = 1 required>
        <label for = '1'>1 star *</label><br>

        <input class = "form-check-input" id = "2" type = "radio" name = "numStars" value = 2 required>
        <label for = '2'>2 stars **</label><br>  
    
        <input class = "form-check-input" id = "3" type = "radio" name = "numStars" value = 3 required>
        <label for = '3'>3 stars ***</label><br>  

        <input class = "form-check-input" id = "4" type = "radio" name = "numStars" value = 4 required>
        <label for = '4'>4 stars ****</label><br>
    
        <input class = "form-check-input" id = "5" type = "radio" name = "numStars" value = 5 required>
        <label for = '5'>5 stars *****</label><br>  
    </fieldset> 
    <input type="submit" class='btn btn-success col-1' value="Submit">
    </form>

    <?php     
    
    // calls the function to add the movie review to the database
    if (!empty($reviewText)) {
        addMovieReview($conn, $reviewText, $numStars);
    }
    
    // gives user a chance to delete movie review
    if(isset($_GET['deleteItemId'])) {
        deleteMovieReview($conn, $_GET['deleteItemId']);
    }

    //function call to print the movie review
    printMovieReview($conn);
    
    //function to add a Movie Review
    function addMovieReview($conn, $reviewText, $numStars) {
        $addReview = "INSERT INTO Reviews (reviewText, numStars)
        VALUES (:reviewText, :numStars)";
        $stmt = $conn->prepare($addReview);
        $stmt->bindParam(':reviewText', $reviewText);
        $stmt->bindParam(':numStars', $numStars);
        $stmt->execute();
    }
    
    //function to delete a movie review
    function deleteMovieReview($conn, $reviewId) {
        $delete = "DELETE FROM reviews WHERE reviewId =:reviewId";
        $stmt = $conn->prepare($delete);
        $stmt->bindParam(':reviewId', $reviewId);
        $stmt->execute();
    }

    //function to print the information to the screen and add the functionality of the delete button
    function printMovieReview ($conn) {
        $selectItem = "SELECT * FROM reviews";
        $stmt = $conn->prepare($selectItem);
        $stmt->execute();
 
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        
        foreach($stmt->fetchAll() as $listRow) {
            echo "<div class='movieReview row'>";
            $reviewId = $listRow['reviewId'];
            $movieReview = $listRow['reviewText'];
            $starRating = $listRow['numStars'];
            echo "<p class='col-4 offset-1'>$movieReview</p>";
            
            if ($starRating === 1) {
                echo "
                <br><p class='col-2'>*</p>
                <a class='btn btn-success col-1' href='reviews.php?deleteItemId=$reviewId'>Delete</a></br>";
            } else if ($starRating === 2) {
                echo "
                <br><p class='col-2'>**</p>
                <a class='btn btn-success col-1' href='reviews.php?deleteItemId=$reviewId'>Delete</a></br>";
            } else if ($starRating === 3) {
                echo "
                <br><p class='col-2'>***</p>
                <a class='btn btn-success col-1' href='reviews.php?deleteItemId=$reviewId'>Delete</a></br>";
            } else if ($starRating === 4) {
                echo "
                <br><p class='col-2'>****</p>
                <a class='btn btn-success col-1' href='reviews.php?deleteItemId=$reviewId'>Delete</a></br>";
            } else if ($starRating === 5) {
                echo "
                <br><p class='col-2'>*****</p>
                <a class='btn btn-success col-1' href='reviews.php?deleteItemId=$reviewId'>Delete</a></br>";
            } 
        echo "</div>";
        }
    }      
?>