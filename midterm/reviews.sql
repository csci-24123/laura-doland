/*CREATE DATABASE midtermlaura;*/
USE midtermlaura;
 
DROP TABLE IF EXISTS Reviews;
CREATE TABLE Reviews (
  reviewId INT PRIMARY KEY AUTO_INCREMENT,
  reviewText VARCHAR(255), 
  numStars int
);
 
INSERT INTO Reviews (reviewText, numStars)
VALUES
("Driving Miss Daisy", 1),
("Pretty Woman", 5);

select * from Reviews where numStars <3;