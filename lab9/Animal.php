<?php
class Animal {
    // Properties
    public $name;
    public $age;

    function __construct($name, $age) {
        $this->name = $name;
        $this->age = $age;
    }

    function __destruct() {
        echo "$this->name is no longer being used, so it's getting destroyed </br>";
    }

    // Methods
    function set_name($name) {
        $this->name = $name;
    }
    function set_age($age) {
        $this->age = $age;
    }
    function get_name() {
        return $this->name;
    }
    function get_age() {
        return $this->age;
    }
    function echoNameAndAge() {
        echo "$this->name is $this->age years old </br>";
    }
}