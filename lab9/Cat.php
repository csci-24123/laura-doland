<?php
include("Animal.php");
class Cat extends Animal {
    public function amountOfTimeSpentSleeping() {
        $sleepingTime = $this->age * 365 * 15;
        echo "The cat $this->name has spent $sleepingTime hours sleeping.</br>";
    }
}
$belle = new Cat("Belle", 5);
$belle->amountOfTimeSpentSleeping();
?>