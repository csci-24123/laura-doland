<?php
include("Animal.php");
class Dog extends Animal {
    public function convertAge() {
        $convertedAge = $this->age * 7;
        echo "The dog $this->name is $this->age years old, which is $convertedAge dog years!</br>";
    }
}
$spot = new Animal('Spot', 3);
$fido = new Animal("Fido", 7);
$asia = new Animal("Asia", 12);
$mia = new Animal("Mia", 6);
$pochi = new Dog('Pochi', 7);

$spot->echoNameAndAge(); 
$fido->echoNameAndAge(); 
$asia->echoNameandAge();
$mia->echoNameAndAge(); 
$pochi->convertAge();
?>   