<?php
class CashRegister {
    // Properties
    protected $amountInRegister;
    
    function __construct($amountInRegister) {
        $this->amountInRegister = $amountInRegister;
    }

    function __destruct() { }
    
    // Methods
    function set_amountInRegister($amountInRegister) {
        $this->amountInRegister = $amountInRegister;
    }
    function get_amountInRegister() {
        return $this->amountInRegister;
    }
    function addMoney($additionalMoney) {
        $this->amountInRegister = $this->amountInRegister + $additionalMoney;
        return $this->amountInRegister;
    }
    function removeMoney($lessMoney) {
        if ($lessMoney > $this->amountInRegister) {
            echo "You cannot remove that much money.";
        } else {
            $this->amountInRegister = $this->amountInRegister - $lessMoney;
            return $this->amountInRegister;
        }
    }
}
?>