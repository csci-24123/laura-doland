<?php
  //echo "Hello world!";
  $firstName = "Laura"; // string
  $lastName = "Doland"; // string
  $age = 46; // integer
  $currentYear = 2022; // integer
  $isMarried = true; // boolean

  echo "$firstName $lastName </br>";
  echo $firstName . " freaking " . $lastName . "</br>";
  echo "I was born in " . $currentYear - $age ." </br>";
  echo "I am married: $isMarried </br>"; // this will print a 1 or a 0.  i means true and 0 means false
  echo "My name is $firstName </br>";
  echo $firstName . " blah " . $lastName . "<br>"; // these will print the same thing.
  $birthYear = $currentYear - $age;
  echo $birthYear . "</br>";
  echo $currentYear - $age . "<br>"; // these will print the same thing.
  $fullName = $firstName . " " . $lastName;
  echo $fullName . "</br>";
  echo "$firstName $lastName </br>"; // these will print the same thing.
  echo 8  % 3 . "</br>"; // 2
  echo 15 % 4 . "</br>"; // 3
  echo 4 % 9 . "</br>"; // 4
  echo 7 % 2 . "</br>"; // 1

  $generation = " "; // set generation to an empty string
  if ($birthYear < 1965) {
    $generation = "Booner";
  } elseif ($birthYear >= 1965 && $birthYear < 1981) {
    $generation = "Gen X";
  } elseif ($birthYear >= 1981 && $birthYear < 1996) {
    $generation = "Millenial";
  } elseif ($birthYear > 1996 && $birthYear < 2012) {
    $generation = "Gen Z";
  } else {
    $generation = "Too young for a generational label";
  }
  echo $generation ."</br>";