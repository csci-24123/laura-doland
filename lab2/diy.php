<?php
$myHobby = "volunteering"; // string
$favoriteVolunteerPlace = "Church"; // string
$NumberofYears = 12; // integer
$yearStarted = 2004; // integer 
$volunteerWithFamily = true; // boolean

/*
I have taught Sunday school for 12 years, but I took a few years off so it's spread out since 2004.
My favorite years were the four years my daughter was in high school and helped
as well as this year when I have been helping my mom.
*/ 

$number = 3;
if ($number %3 === 0 && $number %5 === 0) {
    echo "Divisible by both 3 & 5</br>";
}elseif ($number %5 === 0) {
    echo "Divisible by 5</br>";
}elseif ($number %3 === 0) {
    echo "Divisible by 3</br>";
}else {
    echo "Not divisible by 3 or 5 </br>";
}