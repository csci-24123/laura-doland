<html>
    <?php
        include("header.php");
    ?>
    <body>
        <h1>Welcome to the Fuel Calculator</h1>
        <?php
        
        // Initializing form Variables 
        $averageMilesDriven = "";
        $milesPerGallon = "";
        $weeklyGroceryBudget = "";
        $gasPrice = "";
        
        // Initializing Variables for Errors
        $averageMilesDrivenErr = "";
        $milesPerGallonErr = "";
        $typeOfGasRequiredErr = "";
        $numErrAMD = "";
        $numErrMPG = "";
        $numErrWGB = "";

        // Initializing variable for the price of gas and calculating the total discount
        $priceOfGas = "";
        $totalDiscount = "";
        $finalPricePerGallon = "";

        // statements to collect data from the form
        if ($_SERVER["REQUEST_METHOD"] == "POST"){
            
            if (empty($_POST["averageMilesDriven"])) {
                $averageMilesDrivenErr = "Average Miles Driven is required";
            } elseif (!is_numeric($_POST["averageMilesDriven"])) {
                    $numErrAMD = "Input must be a number";
            } else {
                $averageMilesDriven = clean_input($_POST["averageMilesDriven"]);
            }
            
            if (empty($_POST["milesPerGallon"])) {
                $milesPerGallonErr = "Your cars miles per gallon is required";
            } elseif (!is_numeric($_POST["milesPerGallon"])) {
                    $numErrMPG = "Input must be a number";
            } else {
                $milesPerGallon = clean_input($_POST["milesPerGallon"]);
            }
            
            if (!is_numeric($_POST["weeklyGroceryBudget"])) {
                    $numErrWGB = "Input must be a number";
                } else {
                $weeklyGroceryBudget = clean_input($_POST["weeklyGroceryBudget"]);
            }
            
            if (empty($_POST["gasPrice"])) {
                $typeOfGasRequiredErr = "The type of gas is required";
            } else {
                $gasPrice = clean_input($_POST["gasPrice"]);
            }
        }
    
        
        //function to clean up the data from the form
        function clean_input($data) {
            $data = trim($data); // removes whitespace
            $data = stripslashes($data); // strips strips slashes
            $data = htmlspecialchars($data); // replaces html chars
            return $data;
        }
        // function to calulate the discount with the information provided by the user in the form
        function calculateDiscount($weeklyGroceryBudget) {
            if($weeklyGroceryBudget) {
                $totalDiscount = (floor($weeklyGroceryBudget / 100)) * .10;
            return $totalDiscount; 
            }
        }        

        // function to calculate the final total cose
        function calculateTotalCost($averageMilesDriven, $milesPerGallon, $gasPrice, $totalDiscount) {
        $finalPricePerGallon = ($averageMilesDriven / $milesPerGallon) * ($gasPrice - $totalDiscount);
        }
        ?>

        <style>
            .error {color:#FF0000;}
        </style>
        
        <p><span class = "error">* required field </span></p>
        
        <form method = "post" action = "<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        
            <label for = "averageMilesDriven">Average Miles Driven:</label>
            <input type = "text" name = "averageMilesDriven" id = "averageMilesDriven" required>
            <span class = "error">* <?php echo $averageMilesDrivenErr;?></span>
            <span class = "error"> <?php echo $numErrAMD; ?> </span><br><br>
        
            <label for = "milesPerGallon">Your car's miles per gallon: </label>
            <input type = "text" name = "milesPerGallon" id = "milesPerGallon" required>
            <span class = "error">* <?php echo $milesPerGallonErr;?></span>
            <span class = "error"> <?php echo $numErrMPG; ?> </span><br><br>

            <label for = "weeklyGroceryBudget">What is your weekly grocery budget:</label>
            <input type = "text" name = "weeklyGroceryBudget" id = "weeklyGroceryBudget">
            <span class = "error"> <?php echo $numErrWGB; ?> </span><br><br>

        <fieldset class = "form-check">
            <legend>Type and cost of gas: <span class = "error">*</span></legend>

            <input class = "form-check-input" id = "87" type = "radio" name = "gasPrice" value = 1.89 required>
            <label for = '87'>87 octane - 1.89 $/gal</label><br>

            <input class = "form-check-input" id = "89" type = "radio" name = "gasPrice" value = 1.99 required>
            <label for = '89'>89 octane - 1.99 $/gal</label><br>  
            
            <input class = "form-check-input" id = "92" type = "radio" name = "gasPrice" value = 2.09 required>
            <label for = '92'>92 octane - 2.09 $/gal</label><br>  
    </fieldset> 
            
            <input type = "submit" value = "Submit"> <br> <br>

            <p>Your Average Miles Driven is: <?php echo $averageMilesDriven; ?></p>
            <p>Your car's Miles Per Gallon is: <?php echo $milesPerGallon; ?></p>
            <p>Your weekly grocery budget is: <?php echo $weeklyGroceryBudget; ?></p>
            <p>The price of gas before discount is: <?php echo $gasPrice; ?></p>        
            <p>Your calculated discount is: <?php echo totalDiscount($weeklyGroceryBudget); ?> </p>
            <p>Your total cost is: <?php echo calculateTotalCost($averageMilesDriven, $milesPerGallon, $gasPrice, $totalDiscount); ?> </p>        
    </body>
</html>

