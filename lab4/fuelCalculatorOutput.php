<html>
    <body>
        <p>Welcome</p>
        <p>Your Average Miles Driven is: <?php echo htmlspecialchars($_POST["averageMilesDriven"]); ?></p>
        <p>Your car's Miles Per Gallon is: <?php echo htmlspecialchars($_POST["milesPerGallon"]); ?></p>
        <p>Your weekly grocery budget is: <?php echo htmlspecialchars($_POST["weeklyGroceryBudget"]); ?></p>
        <p>Your gas choice is: <?php echo htmlspecialchars($_POST["Type and Cost of Gas Required"]); ?></p>
    </body>
</html>